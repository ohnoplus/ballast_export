function [Qup,Qrem,Qsed,dQremdb,SHELF] = qbio(grid,M3d,b,nplayers,V,SHELF)

% outputs:
% Qup and Qrem and Qsed: unitless 
% such that Jup = Qup*prod and Jrem = Qrem*prod
% and Jsed = Qsed*(Jup-Jrem)
% where [Jup] = [Jrem] = [Jsed] = mol/m^3/yr and [prod] = mol/m^3/yr

% size of matrix
[ny,nx,nz] = size(M3d);

% index of ocean grid boxes
iocn = find(M3d(:)==1);
isurf = find(M3d(:,:,1)==1);
m = length(iocn);

% make SHELF if it was not provided
if nargin<6
    % give every point an index
    i3d = M3d*0; i3d(:) = 1:(nx*ny*nz);
    % find bottom boxes
    IBOT = repmat(max(i3d.*M3d,[],3),[1 1 nz]);
    ibot = find(i3d>=IBOT);
    % make SHELF
    SHELF = M3d*0; SHELF(ibot) = 1;
end    

% matrix operator to grab grid points adjacent to ocean floor
FB = d0(SHELF(iocn));

% compensation depth
zc = grid.zw(nplayers+1);

% depth of grid boxes counting from surface (zw = 0) to bottom
ZW = cat(3,grid.ZW3d,grid.ZW3d(:,:,end)+grid.DZT3d(:,:,end));
ZW(:,:,1:nplayers) = zc;

% thickness of grid boxes
dzt = grid.dzt;

% source/sink operators for POM and derivatives
QPOM = sparse(nx*ny*nz,nx*ny*nz);
dQPOMdb = sparse(nx*ny*nz,nx*ny*nz);

% POM flux profile
POMF = (ZW./zc).^-b; % unitless

% derivative with respect to b
dPOMFdb = -((ZW./zc).^-b).*log(ZW./zc).*b;

% particle flux divergence [m^-1]
PFD = -diff(POMF,1,3)./grid.DZT3d;
dPFDdb = -diff(dPOMFdb,1,3)./grid.DZT3d;

% interior connectivity to production in kth layer
Ix = speye(nx);
Iy = speye(ny);
PPOM = sparse(nz,nz);
for i = 1:nplayers
    PPOM(i+1:nz,i) = grid.dzt(i);
end
CCPOM = kron(PPOM,kron(Ix,Iy));

% bottom connectivity to surface grid points
PSED = sparse(nz,nz);
PSED(:,1) = 1;
CSED = kron(PSED,kron(Ix,Iy));
CSED = CSED(iocn,iocn);

% source/sink operator for POM
QPOM = d0(PFD)*CCPOM;

% derivative with respect to b
dQPOMdb = d0(dPFDdb)*CCPOM;

% production points and remineralization points
QU = 0*M3d;
QU(:,:,1:nplayers) = 1;
QR = 1-QU;

% uptake operator
Qup = d0(QU(iocn));

% water-column remineralization operator
Qrem = d0(QR)*QPOM;
Qrem = Qrem(iocn,iocn);

% sedimentary remineralization operator
Qsed = d0(1./V)*FB*CSED*CSED'*d0(V);

% derivative with respect to b
dQremdb = d0(QR)*dQPOMdb;
dQremdb = dQremdb(iocn,iocn);
