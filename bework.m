% ran everything in '/jetwork2/cram/PRiSM/staticfloor' up untill 
%'par.02star = ...'

addpath('/jetwork2/cram/PRiSM/staticfloor')
addpath('/jetwork2/cram/ballast_export')
cd('/jetwork2/cram/ballast_export')

sperd = 86400;
dpery = 365.25;
spery = sperd*dpery;

%load stuffs
% load grid and transport opuerator
gpath = 'BGRID';
tpath = 't+s+14c+cfc11';
apath = 'REDI';
spath = 'sol1';
loadpath = sprintf('/graid1/tdevries/TRANSPORT/%s/%s/%s/%s.mat',...
                   gpath,tpath,apath,spath);
load(loadpath,'output')
A = spery*output.TR;
grid = output.grid;
M3d = output.M3d;
iocn = find(M3d(:)==1);

woa = load('/jetwork2/cram/PRiSM/data/woa09obs.mat');
par.po4star = woa.po4an(iocn);
par.o2star = 1.009*woa.o2an(iocn)-2.523; % Bianchi et al. correction

% one round of circulation, basicly for practice
zthresh = 80; % "Surface is less than 80m"

VOL = grid.DXT3d.*grid.DYT3d.*grid.DZT3d;
V = VOL(iocn); 
% shelf locations
[ny,nx,nz] = size(M3d);
TOPO = sum(M3d,3);
TOPO3 = repmat(TOPO,[1 1 nz]);
I3 = 0*M3d;
for k = 1:nz
    I3(:,:,k) = k;       
end
tmp = sign(TOPO3-I3);
SHELF = 0*M3d;
SHELF(tmp(:)==0) = 1;

%% Original mechanism

% layers.I = find(grid.zw < zthresh);

% vec.zw = grid.ZW3d(iocn);
% vec.is.I = vec.zw <= zthresh;
% vec.is.B = vec.zw > zthresh;



% idx.I = find(vec.is.I);
% idx.B = find(vec.is.B);

% vec.P.I = par.po4star(idx.I);
% vec.P.B = par.po4star(idx.B);

% AA.I = A(idx.I, idx.I);
% AA.B = A(idx.I, idx.B);

% novaterm.I = AA.I * vec.P.I;
% novaterm.B = AA.B * vec.P.B;
% next.I = novaterm.I + novaterm.B;

% % testing out dXdt with PO4
% par1.xI = vec.P.I;
% par1.xB = vec.P.B;
% %par1.obs.xI = par.po4star(idx.I);
% %par1.obs.xB = par.po4star(idx.B);
% par1.AA.I = AA.I;
% par1.AA.B = AA.B;
% par1.tau = 1/12;
% xI = par1.xI;

% [test.F test.J] = dXdt(par1.xI, par1);

% % newton solver

% options.atol = 5e-9; options.rtol = 1e-22; options.iprint = 1;
% options.maxit = 40; options.rsham = 0.8;
 

% % get newton solved tracer distribution XO
% [xO, xerr] = nsnew(xI, @(xxI)dXdt(xxI, par1), options);

% % use dXdt to get J
% [xOO, dxOO, JO] = dXdt(xO, par1);

% % Re-assemble J back into the surface ocean.
% vec.P.J = repmat(NaN, size(iocn));
% vec.P.J(idx.I) = JO;
% %however, indexing apparently works where Z is last, so idx.I is
% %just the first set of things
% block.P.J = grid.ZW3d*NaN;
% block.P.J(iocn) = vec.P.J;

% zoneI.P.J = block.P.J(:,:, layers.I)

% export.P = sum(zoneI.P.J, 3);

% %% Silicate
% %woa09 = load('/jetwork2/cram/PRiSM/data/woa09obs.mat')
% parSi = par1;
% par.sistar = woa.sian(iocn);
% vec.Si.I = par.sistar(idx.I);
% vec.Si.B = par.sistar(idx.B);

% parSi.xI = vec.Si.I;
% parSi.xB = vec.Si.B;
% [test.F test.J] = dXdt(parSi.xI, parSi);

% [SixO, Sierr] = nsnew(parSi.xI, @(xxI)dXdt(xxI, parSi), options);
% [SixOO, SidxOO, SiJO] = dXdt(SixO, parSi);

% % what I really want in life is a funciton that does all this for
% % me, so I don't have to do typing gymnastics every time.
% vec.Si.J = repmat(NaN, size(iocn));
% vec.Si.J(idx.I) = SiJO;
% %however, indexing apparently works where Z is last, so idx.I is
% %just the first set of things
% block.Si.J = grid.ZW3d*NaN;
% block.Si.J(iocn) = vec.Si.J;

% zoneI.Si.J = block.Si.J(:,:, layers.I);
% export.Si = sum(zoneI.Si.J, 3);

%% Function approach
par0.A = A;
par0.grid = grid;
par0.M3d = M3d;
iocn = find(M3d(:)==1);
par0.zthresh = zthresh;
par0.tau = 1/12;

export.PO4 = field2ex(woa.po4an, par0);
export.opal = field2ex(woa.sian, par0);

%% now what did I do with that alkalinity data...
in = '/graid1/DATA/Glodap/Alk.nc';
ncid = netcdf.open(in, 'NC_NOWRITE');
[ndim, nvar, natt, unlim] = netcdf.inq(ncid);
for d = 1:ndim
    [dimname, dimlength] = netcdf.inqDim(ncid, d-1);
end
for v = 1:nvar
    [varname, xtyppe, dimid, natt] = netcdf.inqVar(ncid, v-1);
end
Alk.lon = netcdf.getVar(ncid, 0, 0, 360);
Alk.lat =  netcdf.getVar(ncid, 1, 0, 180);
Alk.depth = netcdf.getVar(ncid, 2, 0, 33);
Alk.data =  netcdf.getVar(ncid, 3, [0 0 0], [180 360 33]);

% Alkon, intermediate alkalinity products
% Replace NaN values
Alkon.wna = Alk.data;
% oz = Alkon(:) == -999;
% fna = find(oz);
% fna =  find(Alkon(:) == -999);
% Alk.data(fna) = NaN;
Alkon.wna(Alkon.wna == -999) = NaN;
Il = find(Alk.lon<0);
Ih = find(Alk.lon>0);

Alkon.cat = cat(2, Alkon.wna(:, Ih, :), Alkon.wna(:,Il, :));
Alkon.lon = [Alk.lon(Ih); Alk.lon(Il) + 360];

% Alk.grid.X3d = repmat(Alk.lat, 1, length(Alk.lon), length(Alk.depth)); 
% Alk.grid.Y3d = repmat(Alk.lon', length(Alk.lat), 1, length(Alk.depth)); 
% Alk.grid.Z3d = repmat(permute(Alk.depth, [2 3 1] ), length(Alk.lat), ...
%                       length(Alk.lon), 1);

[Alk.grid.X3d, Alk.grid.Y3d, Alk.grid.Z3d] = meshgrid(Alkon.lon, ...
                                                  Alk.lat, ...
                                                  Alk.depth);




Alkon.regrid = interp3(Alk.grid.X3d, Alk.grid.Y3d, Alk.grid.Z3d, ...
                     Alkon.cat, grid.XT3d, grid.YT3d, grid.ZT3d);
nz = length(grid.zt);

Alkon.repaint = double(NaN*Alkon.regrid);
for z = 1:nz
    Alkon.repaint(:,:,z) = inpaint_nans(double(Alkon.regrid(:,:,z)), ...
                                      5);
end

M3dNaN = M3d;
M3dNaN(M3dNaN == 0) = NaN;
Alkon.repaint = M3dNaN .* Alkon.repaint;

% Not standardized yet
export.Alk = field2ex(Alkon.repaint, par0);
%%

subplot(221); pcolor(export.PO4(:,:,1)); shading flat; colorbar;
subplot(222); pcolor(export.opal(:,:,1)); shading flat; colorbar;
subplot(223); pcolor(export.Alk(:,:,1)); shading flat; colorbar;

% Now I need to think about units.
% Alkalinity umol/kg
% Silicate and phosphorous are umol/L.
% So nearly the same units, I might even not try to convert. Or I
% could do a water density map.

% CaCO3 flux is 1/2 the potential alkalinity flux
% Potential Alkalinity = (Alk + NO3) + 35/S
% Alk (uM) = Alk(umol/kg) * seawater density (kg/L)

% Seawater density 3d map

addpath('/graid1/frenzel/matlab')

ny = length(grid.yt);
rz = repmat(grid.zt', 1, ny);
pres0 = sw_pres(rz, grid.yt);
pres03d = repmat(pres0, 1,1,length(grid.xt));
pres3d = permute(pres03d, [2 3 1]);
woa2 = load('/jetwork2/tweber/Data/BGRID/woa09_ann.mat');
dens3d = real(sw_dens(woa2.saltobs, woa2.tempobs, pres3d));

Alkon.molar = Alkon.repaint.*dens3d/1000;

Alkon.PAlk = (Alkon.molar + woa2.no3obs) .* (35/woa2.saltobs);

export.PAlk = field2ex(Alkon.PAlk, par0);
export.CaCO3 = 0.5 * export.PAlk;

subplot(221); pcolor(export.PO4(:,:,1)); shading flat; colorbar;
title('PO4')
subplot(222); pcolor(export.opal(:,:,1)); shading flat; colorbar;
title('Si(OH)_4')
subplot(223); pcolor(export.Alk(:,:,1)); shading flat; colorbar;
title('Alkalinity Export')
subplot(224); pcolor(export.CaCO3(:,:,1)); shading flat; colorbar;
title('CaCO_3 Export')

subplot(221); pcolor(log10(export.PO4(:,:,1)+1)); shading flat; colorbar;
title('PO4')
subplot(222); pcolor(log10(export.opal(:,:,1)+1)); shading flat; colorbar;
title('Si(OH)_4')
subplot(223); pcolor(log10(export.Alk(:,:,1)+1)); shading flat; colorbar;
title('Alkalinity Export')
subplot(224); pcolor(log10(export.CaCO3(:,:,1)+1)); shading flat; colorbar;
title('CaCO_3 Export')



%% Carbon export
% Mean surface phosphate
msPO4 = nanmean(woa.po4an(:,:,1:3),3);
% Galbraith 2015
p2c = msPO4.*(6.9/1000) + (6/1000);
c2p = 1./p2c;
export.C = export.PO4 .* c2p;

subplot(221); pcolor(export.PO4); shading flat; colorbar;title('PO4 Export')
subplot(222); pcolor(export.C); shading flat; colorbar;title('POC Export')
subplot(223); pcolor(c2p); shading flat; colorbar;title ('C:P')
subplot(224); pcolor(msPO4); shading flat; colorbar; title('ambient PO4')

save('ballast_export.mat', 'export')

%% New mld approach
mldpath = '/jetwork2/tweber/Data/ISCCP/isccp_clim.mat'
inmld = load(mldpath)
%pcolor(inmld.mld_isccp(:,:,1)); shading flat; colorbar;
mpre.mld = inmld.mld_isccp;
mpre.mld(find(mpre.mld(:) >= 1.000e8)) = NaN;
%pcolor(mpre.mld(:,:,6)); shading flat; colorbar;
mpre.amld = nanmean(mpre.mld, 3);
%figure; pcolor(log10(mpre.amld)); shading flat; colorbar;
[mpre.grid.lon, mpre.grid.lat] = meshgrid(inmld.lon2, inmld.lat2);
mpre.regrid = interp2(mpre.grid.lon, mpre.grid.lat, mpre.amld, ...
                      grid.XT, grid.YT);

mld = mpre.regrid;
clear mpre


[Qup, Qrem] = qbio(grid, M3d, 0.86, 3, V, SHELF);
clear Qup
par0.mld = mld;
par0.Qrem = Qrem;

export.PO4 = field2exB(woa.po4an, par0);