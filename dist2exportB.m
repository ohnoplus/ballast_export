% convert distribution map of a chemical tracer to an export map
% using diagnostic method and netwon solver
function[EX] = dist2exportB(mapX, par0)
% output: EX, 2D export map of chemical of interest
% input: mapX 3D distribution of chemical of interest
% input: par0 other parameters

A = par0.A;
grid = par0.grid;
M3d = par0.M3d;
iocn = find(M3d(:)==1);
zthresh = par0.zthresh;
tau = par0.tau;
mld = par0.mld;
Qrem = par0.Qrem

keyboard
%iml = find(

vec.X.all = mapX(iocn);

layers.I = find(grid.zw < zthresh);
layers.I = find(grid.zw < zthresh);

vec.zw = grid.ZW3d(iocn);
vec.is.I = vec.zw <= zthresh;
vec.is.B = vec.zw > zthresh;

idx.I = find(vec.is.I);
idx.B = find(vec.is.B);

vec.X.I = vec.X.all(idx.I);
vec.X.B = vec.X.all(idx.B);

AA.I = A(idx.I, idx.I);
AA.B = A(idx.I, idx.B);

QQ.I = Qrem(idx.I, idx.I);
QQ.B = Qrem(idx.I, Idx.B);

novaterm.I = AA.I * vec.X.I;
novaterm.B = AA.B * vec.X.B;
novaterm.Q = QQ.I * vec.X.I;
next.I = novaterm.I + novaterm.B + novaterm.Q;

% testing out dXdt with PO4
par1.xI = vec.X.I;
par1.xB = vec.X.B;
%par1.obs.xI = par.po4star(idx.I);
%par1.obs.xB = par.po4star(idx.B);
par1.AA.I = AA.I;
par1.AA.B = AA.B;
par1.QQ.I = QQ.I;
par1.tau = 1/12;

options.atol = 5e-9; options.rtol = 1e-22; options.iprint = 1;
options.maxit = 40; options.rsham = 0.8;
 
keyboard
% get newton solved tracer distribution XO
[xO, xerr] = nsnew(par1.xI, @(xxI)dXdtB(xxI, par1), options);

% use dXdt to get J
[xOO, dxOO, JO] = dXdt(xO, par1);

% Re-assemble J back into the surface ocean.
vec.X.J = repmat(NaN, size(iocn));
vec.X.J(idx.I) = JO;
%however, indexing apparently works where Z is last, so idx.I is
%just the first set of things
block.X.J = grid.ZW3d*NaN;
block.X.J(iocn) = vec.X.J;

zoneI.X.J = block.X.J(:,:, layers.I);

export.X = sum(zoneI.X.J, 3);
EX = export.X;