% simple circulation and restoring operation
function [F, FP, J] = dXdtB(xI, par)
% F is the time tendency of the tracer in the surface
% FP is the derivative of F
% J is the distance of the tracer from equilibrium over the
% damping time scale

% xI is the distribution of the tracer in the surface
% par is a bunch of stuff

% hyperbolic tangent madness constant
ht = 1000;

% xB is the distribution of that tracer in the deep
xB = par.xB;

% observed distributions the tracer in the surface and in the deep
obs.xI = par.xI;
obs.xB = par.xB;

% Matrix of surface advection
AA.I = par.AA.I;

% Matrix of advection of deep water into the surface
AA.B = par.AA.B;

% Reminerilize surface export into the deep mixed layer
QQ.I = par.QQ.I;

% damping timescale
tau = par.tau;

mJ = (xI - obs.xI)/tau;
% deal with negatives

nJ = 0.5*tanh(ht*(xI-obs.xI))+.5;
J = mJ .* nJ;

dnJdx = -.5*ht*(tanh(ht*(xI-obs.xI)).^2-1);
dmJdx = ones(length(xI),1)/tau;
dJdx = mJ.*dnJdx + nJ.*dmJdx;

% F = dfdt
F = AA.I * xI + AA.B * xB + QQ.I * xI - J;

dFdx = AA.I + QQ.I - d0(dJdx);
% why isn't AA.B in here. Do I need QQ.I?

if nargout>=2 % factor Jacobian
    FP = mfactor(dFdx);
end
keyboard